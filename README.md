
assets/                 - zawiera pliki publiczne

assets/build            - abudowane i zoptymalizowane pliki (w tym obrazki - w projekcie akurat nie wykorzystałem). Budowane grunt build

assets/app              - zawiera aplikację angularową

assets/index.html       - głowny plik html projektu

assets/app/test         - zawiera testy angulara

assets/app/mock         - mocki dla dostępu do webapi

assets/app.js           - główny plik aplikacji

assets/app-bootstrap.js - odpala aplikację

assets/test-main.js     - główny plik testów

aplikację możemy odpalić za pomoca polecenia "sails lift" - wymaga instalacji dependecies