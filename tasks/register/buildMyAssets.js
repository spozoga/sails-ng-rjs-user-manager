module.exports = function (grunt) {
	grunt.registerTask('buildMyAssets', [
    'webfont',
    'sprite',
    'sass',
    'requirejs'
	]);
};
