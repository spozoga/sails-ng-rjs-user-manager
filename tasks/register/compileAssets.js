module.exports = function (grunt) {
	grunt.registerTask('compileAssets', [
		'clean:dev',
    'buildMyAssets',
		'jst:dev',
		'less:dev',
		'copy:dev',
		'coffee:dev'
	]);
};
