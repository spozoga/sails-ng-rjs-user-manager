

module.exports = function (grunt) {

  grunt.config.set('sass', {
      dist: {
        files: [{
          expand: true,
          cwd: 'assets/styles/sass/',
          src: ['*.scss'],
          dest: 'assets/build/sass/',
          ext: '.css'
        }]
      }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
};
