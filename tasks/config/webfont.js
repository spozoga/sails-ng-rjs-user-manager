module.exports = function (grunt) {

  grunt.config.set('webfont', {
    icons: {
      src: 'assets/resources/img/icons-svg/*.svg',
      dest: 'assets/build/webfont',
      options: {
        engine: 'node',
        stylesheet: 'css',
        ie7: true,
        autoHint: false,
        embed: true,
        relativeFontPath: "build/webfont/"
        //types: 'woff',
      }
    }
  });

  grunt.loadNpmTasks('grunt-webfont');
};
