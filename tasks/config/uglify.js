/**
 * Minify files with UglifyJS.
 *
 * ---------------------------------------------------------------
 *
 * Minifies client-side javascript `assets`.
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-uglify
 *
 */
module.exports = function(grunt) {

	grunt.config.set('uglify', {
		dist: {
			src: ['assets/app/dist/app.js'],
			dest: 'assets/app/dist/app.min.js'
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
};
