module.exports = function (grunt) {

  grunt.config.set('sprite', {
      all: {
        src: 'assets/resources/img/sprites/*.png',
        dest: 'assets/build/sprite/spritesheet.png',
        destCss: 'assets/build/sprite/sprites.css',
        //cssSpritesheetName: 'sprit',
        cssVarMap: function (sprite) {
          sprite.name = 'sprite-' + sprite.name;
        }
      }
  });

  grunt.loadNpmTasks('grunt-spritesmith');
};
