/**
 * Concatenate files.
 *
 * ---------------------------------------------------------------
 *
 * Concatenates files javascript and css from a defined array. Creates concatenated files in
 * .tmp/public/contact directory
 * [concat](https://github.com/gruntjs/grunt-contrib-concat)
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-concat
 */
module.exports = function(grunt) {

  grunt.config.set('requirejs', {
    compile: {
      options: {
        mainConfigFile: "assets/app/main.js",
        baseUrl: "assets/app",
        include: ["app-bootstrap"],
        name: "../vendors/almond/almond",
        out: "assets/build/requirejs/app.js",
        wrap: true,

        done: function(done, output) {
          var duplicates = require('rjs-build-analysis').duplicates(output);

          if (duplicates.length > 0) {
            grunt.log.subhead('Duplicates found in requirejs build:');
            grunt.log.warn(duplicates);
            return done(new Error('r.js built duplicate modules, please check the excludes option.'));
          }

          done();
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
};
