
module.exports = {

  type: "smtp",

  senderAddress: "Code Builder <info@code-builder.com>",

  transport: {
    host: 'localhost',
    port: 25,
    auth: {
      user: 'username',
      pass: 'password'
    }
  }

};
