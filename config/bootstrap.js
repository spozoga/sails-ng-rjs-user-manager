/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var Q = require('q');

Q.longStackSupport = true;

module.exports.bootstrap = function (cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  //_.extend(sails.hooks.http.app.locals, sails.config.http.locals);

  //load view locals from Services
  var all = [];

  for (var i in sails.services) {
    if (typeof sails.services[i]._exportViewHelpers === 'function') {
      all.push(sails.services[i]._exportViewHelpers());//_.extend(sails.hooks.http.app.locals, sails.services[i].exportViewHelpers());
    }
  }

  Q.all(all).then(function(results){
    for(var i in results){
      _.extend(sails.hooks.http.app.locals, results[i]);
    }
    cb();
  });


};
