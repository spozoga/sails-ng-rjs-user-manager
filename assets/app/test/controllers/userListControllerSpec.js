define(['angular', 'app', 'test/mocks/userDAO'], function(ng, app, userDAOMock) {

    describe('userListController', function() {
        var $injector = ng.bootstrap(document.implementation.createHTMLDocument(), ['app']),
            $controller = $injector.get('$controller'),
            $scope,
            userDAOInstence,
            controller;

        beforeEach(function(){
            //module('app');
            $scope = {};
            userDAOInstence = userDAOMock.build();
            controller = $controller('userListController', { $scope: $scope, userDAO: userDAOInstence.getMock() });
        });

        it('is defined', function() {
            expect(controller).toBeDefined();
        });

        it('load users', function() {
            expect($scope.users.length).toBeGreaterThan(0);
        });

        it('remove user from list', function() {
            var user = $scope.users[0];
            $scope.deleteUser(user);
            //user list don't containt delete user
            for(var i in $scope.users){
                expect($scope.users[i].id).not.toEqual(user.id);
            }
        });

        it('remove user send query', function() {
            var user = $scope.users[0];
            $scope.deleteUser(user);
            expect(userDAOInstence.getAccess().delete).toEqual(true);
        });

        it('save user send query', function() {
            var user = $scope.users[0];
            $scope.saveUser(user);
            expect(userDAOInstence.getAccess().save).toEqual(true);
        });

        it('edit change state to edit', function() {
            var user = $scope.users[0];
            $scope.editUser(user);
            expect(user.$$edited).toEqual(true);
        });

        it('cancel edit roolback changes', function() {
            var oldname, user = $scope.users[0];
            $scope.editUser(user);
            oldname = user.name;
            user.name = 'newname';
            $scope.cancelEditUser(user);
            expect(user.name).toEqual(oldname);
        });

    });

});