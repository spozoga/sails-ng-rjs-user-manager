define(['angular'], function (ng) {
    'use strict';
    var api = {};

    api.build = function() {
        var mock, remove, access, list = [
            {
                "name": "name1",
                "surname": "surname1",
                "dateOfBirth": "2015-11-02T23:00:00.000Z",
                "mobileNumber": "1111",
                "address": "address1",
                "id": 1,
                "createdAt": "2015-03-29T07:42:38.099Z",
                "updatedAt": "2015-03-29T07:42:38.099Z"
            },
            {
                "name": "name2",
                "surname": "surname2",
                "dateOfBirth": "2015-11-02T23:00:00.000Z",
                "mobileNumber": "22222",
                "address": "address2",
                "id": 2,
                "createdAt": "2015-03-29T07:42:40.179Z",
                "updatedAt": "2015-03-29T17:31:20.737Z"
            },
            {
                "name": "name3",
                "surname": "surname3",
                "dateOfBirth": "2015-11-02T23:00:00.000Z",
                "mobileNumber": "3333",
                "address": "address3",
                "id": 3,
                "createdAt": "2015-03-29T07:42:41.088Z",
                "updatedAt": "2015-03-29T07:42:41.088Z"
            }
        ];

        remove = function(params){
            list = list.filter(function (u) {
                return u.id != params.id;
            });
            access.delete = true;
            access.remove = true;
        };

        access = {
            query: false,
            get: false,
            save: false,
            delete: false,
            remove: false
        };

        mock = {
            query: function(){
                access.query = true;
                return list;
            },
            get: function(params){
                access.get = true;
                for(var i in list) if(list[i].id == params.id){
                    return ng.copy(list[i]);
                }
            },
            save: function(params, entity){
                access.save = true;
                for(var i in list) if(list[i].id == params.id){
                    return ng.copy(list[i]);
                }
            },
            delete: remove,
            remove: remove
        };

        return {
            getAccess: function(){
                return access;
            },
            getMock: function(){
                return mock;
            }
        }
    };


    return api;
});
