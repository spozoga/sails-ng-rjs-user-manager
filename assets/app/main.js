require.config({
  // alias libraries paths
  paths: {
    //requirejs
    'domReady': '../vendors/requirejs-domready/domReady',

    //angluar
    'angular': '../vendors/angular/angular',
    'ngRoute': '../vendors/angular-route/angular-route',
    'ngRouter': '../vendors/angular-ui-router/release/angular-ui-router',
    'ngResource': '../vendors/angular-resource/angular-resource',
    'ngMock': '../vendors/angular-mocks/angular-mocks.js',

    //jquery
    'jquery': '../vendors/jquery/dist/jquery',

    //bootstrap
    'bootstrap': '../vendors/bootstrap-sass/assets/javascripts/bootstrap',

    //sails
    'io': './libs/sails.io.js'

    //other
  },
  // angular does not support AMD out of the box, put it in a shim
  shim: {
    //requirejs

    //angluar
    'angular': {
      exports: 'angular'
    },
    'ngRoute': {
      exports: 'ngRoute',
      deps: ['angular']
    },
    'ngRouter': {
      deps: ['angular']
    },
    'ngResource': {
      deps: ['angular']
    },
    'ngMock': {
      deps: ['angular']
    },

    //jquery
    'jquery': {
      exports: 'jquery'
    },

    //bootstrap
    'bootstrap': {
      deps: ['jquery']
    },

    //sails
    'io': {
      exports: 'io'
    }

  }//,
  // kick start application
  //deps: ['./app']
});
