define([
  'angular',
  './services/index',
  './controllers/index',
  './filters/index',
  './directives/index',
  'domReady'
], function (ng) {
  'use strict';

  //Load ng
  var app = ng.module('app', [
    'app.services',
    'app.controllers',
    'app.filters',
    'app.directives'
  ]);

  return app;
});
