define(['./module', 'angular'], function (module, ng) {
    'use strict';
    module.controller('userListController', ['$scope', 'userDAO', function ($scope, userDAO) {
        //default data
        var editBackup = {};
        $scope.users = [];
        $scope.defaulUser = {};


        //user functions
        $scope.editUser = function (user) {
            editBackup[user.id] = ng.copy(user);
            user.$$edited = true;
            user.$$selected = false;
        };

        $scope.editSelectedUser = function (user) {
            for (var i in $scope.users) {
                if ($scope.users[i].$$selected)
                    $scope.editUser($scope.users[i]);
            }
        };

        $scope.cancelEditUser = function (user) {
            var u = editBackup[user.id];
            editBackup[user.id] = null;
            for (var i in u) {
                user[i] = u[i];
            }
            user.$$edited = false;
            user.$$selected = false;
        };

        $scope.saveUser = function (user) {
            userDAO.save({id: user.id}, user);
            user.$$edited = false;
        };

        $scope.deleteUser = function (user) {
            userDAO.delete({id: user.id});
            $scope.users = $scope.users.filter(function (u) {
                return u.id != user.id;
            });
        };


        //create new user functions
        $scope.resetNewUser = function () {
            $scope.newUser = ng.copy($scope.defaulUser);
        };

        $scope.addNewUser = function () {
            var u = userDAO.save($scope.newUser);
            $scope.users.push(u);
            $scope.resetNewUser();
        };


        //load data
        $scope.users = userDAO.query();
        $scope.resetNewUser();
    }]);

    return module;
});
