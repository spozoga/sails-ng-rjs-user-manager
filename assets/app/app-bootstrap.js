require([
  'angular',
  'app',
  'domReady'
], function (ng, app, domReady) {
  'use strict';

  //bootstrap app
  require(['domReady!'], function(document) {
    ng.bootstrap(document, ['app']);
  });

  return app;
});
