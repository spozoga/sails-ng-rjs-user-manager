define(['angular', 'ngResource'], function(ng) {
    'use strict';
    return ng.module('app.services', ['ngResource']);
});