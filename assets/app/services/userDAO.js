define(['./module'], function (module) {
  'use strict';
  module.service('userDAO', ['$resource', function ($resource) {
      return $resource('/rest/users/:id');
  }]);
});
