define(['./module', 'jquery'], function(module, $) {
    'use strict';
    module.directive('appLoaded', [function() {
            return {
                link: function($scope, element, attrs) {
                        $(element).addClass('app-loaded');
                    }
            };
        }]);
});