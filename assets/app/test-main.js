
var TEST_REGEXP = /(spec|test)\.js$/i;

var pathToModule = function(path) {
  return path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

require.config({
  // Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base/app',

  // dynamically load all test files
  deps: ['test/index'],

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
