/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var loginForm = require('../forms/Login');

module.exports = {

  login: function (req, res) {
    if (req.method == "POST") {
      UserService.login(req, req.body.username, req.body.password, function (err) {
        if (err) {
          //.... add message here
          req.flash('error', 'user.login.userNotFoundOrIncorrectPassword');
          //return res.view("user/login");
          module.exports.loginView(req, res);
        } else {
          // redirect to home on success
          res.redirect('/');
        }
      });
      return;
    }

    module.exports.loginView(req, res);
  },

  loginView: function(req, res){
    return RenderService.view(req, res, "user/login", {
      title: 'dupa',
      form: User.attributes
    });
  },

  logout: function (req, res) {
    UserService.logout(req, function (err) {
      if (err) return next(err);
      return res.redirect("/");
    });
  },

  register: function (req, res) {
    if (req.method == "POST") {
      UserService.register(req.body, function (err) {
        if (err) {
          //.... add message here
          req.flash('error', 'user.login.userNotFoundOrIncorrectPassword');
          return res.view("user/login");
        }
        // redirect to home on success
        res.redirect('/');
      });
      return;
    }
    return res.view("user/register", {
      userMeta: userMeta
    });
  }

};

