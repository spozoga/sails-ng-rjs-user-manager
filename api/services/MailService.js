
var
  //dependencies
  fs = require('fs'),
  nodemailer = require('nodemailer'),
  smtpTransport = require('nodemailer-smtp-transport'),
  //config
  mailConfig = require("../../config/mail"),
  //instances
  transport = nodemailer.createTransport(smtpTransport(mailConfig.transport)),
  //variables
  mailTemplates = {};
  fn = {};

// private functions
fn.loadMailTemplates = function(path){
  var txt, txtPath, html, htmlPath;
  if(mailTemplates[path]) return mailTemplates[path];

  txtPath = "/views/"+path+".txt.ejs";
  txt = fs.readFileSync(txtPath, 'utf8');

  htmlPath = "/views/"+path+".html.ejs";
  html = fs.readFileSync(htmlPath, 'utf8');

  return mailTemplates[path] = {
    txt: ejs.compile(txt, {
      filename: txtPath
    }),
    html: ejs.compile(html, {
      filename: htmlPath
    })
  };
};

// public api
exports.sendToUser = function(user, title, template, data, next) {
  return exports.send(user.email, title, template, {
    user: user,
    data: data
  }, next);
};

exports.send = function(emails, title, template, data, next) {
  //render templates
  templates = fn.loadMailTemplates(template);

  //send emails
  transporter.sendMail({
    from: mailConfig.senderAddress,
    to: emails,
    subject: __(title),
    text: templates.txt(data),
    html: templates.html(data)
  }, next);
};
