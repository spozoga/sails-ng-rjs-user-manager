
/*
 *  Vars
 */
var fn = {},
    defaults = {},
    _ = require('underscore');
/*    IGNORED_ATTRS = ["type"],
    HTML5_VALID_ATTRS = ["required", "minLength", "maxLength"],
    SAILS_VALID_ATTRS = ["after", "alpha", "alphadashed", "alphanumeric", "alphanumericdashed", "array", "before", "binary", "boolean", "contains", "creditcard", "date", "datetime", "decimal", "email", "empty", "equals", "falsey", "finite", "float", "hexadecimal", "hexColor", "in", "int", "integer", "ip", "ipv4", "ipv6", "is", "json", "len", "lowercase", "max", "maxLength", "min", "minLength", "not", "notContains", "notEmpty", "notIn", "notNull", "notRegex", "null", "number", "numeric", "object", "regex", "protected", "required", "string", "text", "truthy", "undefined", "unique", "uppercase", "url", "urlish", "uuid", "uuidv3", "uuidv4"];
*/

/*
 *  Default values
 */

defaults.INPUT_TEMPLATE = '<input name="[:name]" ng-model="[:formName]Data.[:name]" type="[:type]" [:attrs] />';
defaults.IGNORED_ATTRS = ["type"];
defaults.HTML5_VALID_ATTRS = ["required", "minLength", "maxLength"];
defaults.SAILS_VALID_ATTRS = ["after", "alpha", "alphadashed", "alphanumeric", "alphanumericdashed", "array", "before", "binary", "boolean", "contains", "creditcard", "date", "datetime", "decimal", "email", "empty", "equals", "falsey", "finite", "float", "hexadecimal", "hexColor", "in", "int", "integer", "ip", "ipv4", "ipv6", "is", "json", "len", "lowercase", "max", "maxLength", "min", "minLength", "not", "notContains", "notEmpty", "notIn", "notNull", "notRegex", "null", "number", "numeric", "object", "regex", "protected", "required", "string", "text", "truthy", "undefined", "unique", "uppercase", "url", "urlish", "uuid", "uuidv3", "uuidv4"];


/*
 *  Helpers
 */

fn.renderAttrValue = function(value){
  if(_.isArray(value) || _.isObject(value)){
    return JSON.stringify(value);
  }
  return value.toString();
}

fn.renderAttr = function(name, body){
  return name + (body===true) ? '' : '="'+fn.renderAttrValue(body)+'"';
}

fn.renderAttrs = function(input){
  attrs = [];
  for(var i in input) {
    if(defaults.IGNORED_ATTRS.indexOf(i)!==-1) continue;
    //HTML5
    if(defaults.HTML5_VALID_ATTRS.indexOf(i)!==-1) {
      attrs.push( fn.renderAttr(i, input[i]) );
      continue;
    }
    //SAILS
    attrs.push( fn.renderAttr('valid-'+i, input[i]) );
  }
  return attrs.join(" ");
}

fn.html5InputType = function(input){
  if(!input) return 'text';
  return input.email? 'email' :
    input.numeric? 'number' :
      input.date? 'datet' :
        input.datetime? 'datetime' : 'text';
}

/*
 * Modeule Exports
 */

exports.renderStartForm = function(req, formName, meta, settings) {
  //init form data
  req.renderData = req.renderData ? req.renderData : {};

  //prevent - open form inside form
  if(req.renderData.form){
    throw "You musn't open form inside form";
  }

  //init data
  req.renderData.form = {
    formName: formName,
    meta: meta ? meta : {},
    settings: settings ? settings : {}
  };

  //render html
  return '<form method="POST" enctype="multipart/form-data" name="'+formName+'">';
};

exports.renderEndForm = function(req) {
  //init form data
  req.renderData = req.renderData ? req.renderData : {};

  //no open multi forms
  if(!req.renderData.form){
    throw "No open form";
  }

  //close
  req.renderData.form = null;

  //render html
  return '</form>';
};

exports.renderInput = function(req, name, html) {
  var inputMeta, type, inputHtml;

  //req data
  req.renderData = req.renderData ? req.renderData : {};

  //form must be open
  if(!req.renderData.form){
    throw "No open any form";
  }

  //process field data
  inputHtml = html ? html : defaults.INPUT_TEMPLATE;
  inputMeta = req.renderData.form.meta[name];
  type = fn.html5InputType(inputMeta);

  //render


  var response = inputHtml
    .replace(/\[:name\]/g, name)
    .replace(/\[:formName\]/g, req.renderData.form.formName)
    .replace(/\[:type\]/g, type)
    .replace(/\[:attrs\]/g, fn.renderAttrs(inputMeta));
  return response;
};

exports.renderFormRow = function(req, name, inputHtml) {
  return exports.renderInput(req, name, "<div class='row'>" +
      "<label>[:formName].[:name]</label>" +
      "<div class='input'>" + (inputHtml ? inputHtml : defaults.INPUT_TEMPLATE) + "</div>" +
      "<div class='errors'></div>" +
    "</div>");
};

exports._initRequestView = function(req) {
  var scope = {};
  return {
    startForm: exports.renderStartForm.bind(scope, req),
    endForm: exports.renderEndForm.bind(scope, req),
    formInput: exports.renderInput.bind(scope, req),
    formRow: exports.renderFormRow.bind(scope, req)
  }
};
