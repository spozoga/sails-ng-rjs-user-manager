
var Q = require("q");

exports.formatDate = function(date) {
  var year, month, day;
  if(!date) date = new Date();
  year = date.getFullYear();
  month = date.getMonth()+1;
  day = date.getDate();
  return year +'-'+ ((month<10)?'0'+month:month) +'-'+ ((day<10)?'0'+day:day);
};

exports._exportViewHelpers = function() {
  return {
    filters:{
      formatDate: exports.formatDate
    },
    formatDate: exports.formatDate
  };
};

