
var _initUserSession, _destroyUserSession, bcrypt = require('bcrypt'), Q = require('q');


exports.genSalt = function(user, next) {
  if(user.salt){
    return next(null, user.salt);
  }
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);
    user.salt = salt;
    return next(null, salt);
  });
};

exports.hashPassword = function(salt, password, next) {
  bcrypt.hash(password, salt, function(err, hash) {
    if (err) return next(err);
    return next(null, hash);
  });
};

exports.setPassword = function(user, password, next) {
  exports.genSalt(user, function (err, salt) {
    if(err) return next(err);
    exports.hashPassword(salt, password, function(err, hash){
      if(err) return next(err);
      user.password = hash;
      return  next();
    });
  })
};

exports.compareUserPassword = function(user, password, next) {
  return bcrypt.compare(password, user.password, next);
};

_initUserSession = function(req, user){
  req.session.userTrack = {
    user:{
      id:  user.id
    }
  };
}

_destroyUserSession = function(req){
  req.session.userTrack = null;
}

exports.login = function(req, id, password, next) {
  User.findOne({
    or : [
      { name: id },
      { email: id }
    ]
  }).exec(function (err, user) {
    if (err || !user) return next(err?err:"No find user");
    exports.compareUserPassword(user, password,function(err, match){
      if (match) {
        _initUserSession(req, user);
        return next(null, user);
      } else {
        _destroyUserSession(req);
        return next(err?err:"Passwords no match");
      }
    });
  });
};

exports.logout = function(req, next) {
  _destroyUserSession(req);
  next();
};

exports.getUserSession = function(req, next) {
  if(!req.session.userTrack){
    return next("No find user session")
  }
  return next(null, req.session.userTrack);
};

exports.register = function(user, next) {
  User.create(user).exec(function(err, user){
    if(err) next(err);
    MailService.sendToUser(user, 'user.register.mail.title', 'user/mail/register_welcome', {});
    next(null, user);
  });
};

exports.loggedInUser = function(req, next) {
  exports.getUserSession(req, function(err, sess){
    if(err) return next(err);
    User.findOneById(sess.user.id).exec(function (err, usr) {
      if (err || !usr) next(err?err:"No find user");
      return next(null, usr);
    });
  });
};

exports._initRequestView = function(req) {
  var deferred = Q.defer();

  exports.loggedInUser(req, function(err, user){
    deferred.resolve({
      app:{
        loggedInUser: user
      }
    });
  });

  return deferred.promise;
};
